/*
 * NetModule interview question
 */

#include <stdio.h>
#include <stdlib.h>

#define max(a, b)       (((a) > (b)) ? (a) : (b))
#define min(a, b)       (((a) < (b)) ? (a) : (b))

/*
 * Bruteforce O(n2) solution
 */
long getMaxProfit(int stockPrices[], size_t numStockPrices) {
        long profit, maxProfit = 0;

        for (int i = 0; i < numStockPrices; i++) {
                // We cannot sell in T1 then buy in T2, so while searching
                // for a selling price, start only from where we are now..
                for (int j = i; j < numStockPrices; j++) {
                        profit = -stockPrices[i];
                        profit += stockPrices[j];
                        maxProfit = max(profit, maxProfit);
                }
        }

        return maxProfit;
}

/*
 * Optimized linear, O(n), solution
 *
 * While iterating the numbers, we are _not_ interested in all n**2
 * combinations. From the past, we're _only_ interested in the minimum
 * price encountered so far. So we can save that, while doing linear
 * iteration.
 *
 * Lesson? Examine what we really want from the past in a n2 solution.
 */
long getMaxProfit_optimized(int stockPrices[], size_t numStockPrices) {
        long minPrice = stockPrices[0];
        long profit, maxProfit = 0;

        for (int i = 1; i < numStockPrices; i++) {
                profit = stockPrices[i] - minPrice;
                maxProfit = max(maxProfit, profit);
                minPrice = min(minPrice, stockPrices[i]);
        }

        return maxProfit;
}

int main(int argc, char **argv) {
        int stockPrices[6] = {10, 7, 5, 8, 11, 9};
        size_t numStockPrices = 6;

        printf("Max profit = %ld$\n",
               getMaxProfit_optimized(stockPrices, numStockPrices));

        return 0;
}
