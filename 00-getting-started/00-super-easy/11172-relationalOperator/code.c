#include <stdio.h>

int main(int argc, char **argv) {
        int sets, n1, n2;

        scanf("%d", &sets);
        while(sets--) {
                scanf("%d %d", &n1, &n2);

                if (n1 < n2)
                        printf("<\n");
                else if (n1 > n2)
                        printf(">\n");
                else
                        printf("=\n");
        }
}

