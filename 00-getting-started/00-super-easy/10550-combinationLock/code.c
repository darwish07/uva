#include <stdio.h>
#include <stdbool.h>

enum direction {
        CLOCKWISE,
        COUNTER_CLOCKWISE,
};

const int full_circle_marks = 40;

int marks_distance(enum direction direction, int from, int to) {
        int distance;

        if (direction == CLOCKWISE) {
                distance = from - to;
                if (distance < 0)
                        distance += full_circle_marks;
        } else {
                distance = to - from;
                if (distance < 0)
                        distance += full_circle_marks;
        }

        return distance;
}

int degrees(int marks) {
        return marks * 9;
}

int main(int argc, char **argv) {
        while (true) {
                int start, n1, n2, n3, marks_total = 0;

                scanf("%d %d %d %d", &start, &n1, &n2, &n3);
                if (start == 0 && n1 == 0 && n2 == 0 && n3 == 0)
                        break;

                marks_total = 2 * full_circle_marks;
                marks_total += marks_distance(CLOCKWISE, start, n1);

                marks_total += full_circle_marks;
                marks_total += marks_distance(COUNTER_CLOCKWISE, n1, n2);

                marks_total += marks_distance(CLOCKWISE, n2, n3);

                printf("%d\n", degrees(marks_total));
        }
}
