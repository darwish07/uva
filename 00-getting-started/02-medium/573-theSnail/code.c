#include <stdio.h>
#include <stdbool.h>

int main(int argc, char **argv) {
        double well_height, snail_height, climb_up, slide_down, fatigue_factor, fatigue_distance;
        long day;

        while (true) {
                scanf("%lf %lf %lf %lf", &well_height, &climb_up, &slide_down, &fatigue_factor);
                if (well_height == 0)
                        break;

                snail_height = 0;
                fatigue_distance = climb_up * (fatigue_factor/100.0);

                for (day = 1; ; day++) {
                        snail_height += climb_up;
                        if (snail_height > well_height) {
                                printf("success on day %ld\n", day);
                                break;
                        }                                

                        snail_height -= slide_down;
                        if (snail_height < 0) {
                                printf("failure on day %ld\n", day);
                                break;
                        }

                        climb_up -= fatigue_distance;

                        /* "The snail never climbs a negative distance. If the fatigue factor
                         * drops the snail’s climbing distance below zero, the snail does
                         * not climb at all that day." Thus, we'll let it slide every day
                         * until it falls off ;-) */
                        if (climb_up < 0)
                                climb_up = 0;
                }
        }

        return 0;
}
