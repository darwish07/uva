#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#define LOAN_MONTHS_MAX 101
double car_depreciation[LOAN_MONTHS_MAX];

int main(int argc, char **argv) {
        int loan_months, n, i;
        double downpayment, loan, monthly_payment;

        while (true) {
                scanf("%d %lf %lf %d", &loan_months, &downpayment, &loan, &n);
                assert(loan_months <= LOAN_MONTHS_MAX);
                if (loan_months < 0)
                        break;

                monthly_payment = loan / (double)loan_months;

                while (n--) {
                        int month;
                        double depreciation;
                        scanf("%d %lf", &month, &depreciation);

                        for (i = month; i < LOAN_MONTHS_MAX; i++)
                                car_depreciation[i] = depreciation;
                }

                /* Find our barrier */
                double loan_remaining = loan;
                double car_value = loan + downpayment;
                assert(car_value <= 75000.0);
                for (i = 0; i <= loan_months; i++) {
                        car_value -= (car_value * car_depreciation[i]);
                        loan_remaining -= (i > 0) ? monthly_payment : 0;
                        if (car_value > loan_remaining) {
                                printf("%d month%s\n", i, (i != 1) ? "s" : "");
                                break;
                        }
                }
        }

        return 0;
}
