#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>

#define HOTELS_MAX      19
#define WEEKENDS_MAX    14

int available_beds[WEEKENDS_MAX];

int main(int argc, char **argv) {
        int nparticipants, budget, nhotels, nweekends;
        int person_price, i, total_cost, min_total_cost;

        while (true) {
                if (scanf("%d %d %d %d", &nparticipants, &budget, &nhotels, &nweekends) == EOF)
                        break;

                min_total_cost = INT_MAX;
                while (nhotels--) {
                        scanf("%d", &person_price);
                        for (i = 0; i < nweekends; i++)
                                scanf("%d", &available_beds[i]);

                        for (i = 0; i < nweekends; i++) {
                                if (available_beds[i] < nparticipants)
                                        continue;

                                total_cost = person_price * nparticipants;
                                if (total_cost > budget)
                                        continue;

                                if (total_cost < min_total_cost)
                                        min_total_cost = total_cost;
                        }
                }

                if (min_total_cost == INT_MAX)
                        printf("stay home\n");
                else
                        printf("%d\n", min_total_cost);
        }

        return 0;
}
