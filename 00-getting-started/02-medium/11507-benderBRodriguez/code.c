#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define flip_sign(str)  { str[0] = (str[0] == '-') ? '+' : '-'; }
#define different_sign(stra, strb) (stra[1] == strb[1] && stra[0] != strb[0])

/*
 * To build a correct simulation, think of wire movements in
 * the 3D sphere. The z axis _is_ the third dimension. That
 * is why "+z +z" moves at the joints lead to "-x", and
 * "+z -z" leads to "x".
 *
 * If you need help, simulate this with a pen and its cover.
 *
 * Also remember that:
 * - "+z" points to 3d-down
 * - While bending at certain joint j: j <= L-1, form a 90
 *   degree angle at _that_ joint, and __set in stone__ all
 *   the 90 degrees angles set before it! (joints x: j+1 < x < L).
 * - When we say bend at joint j, parallel to "y+" for example,
 *   we mean that the __arrow__ formed from j to j+1 points to
 *   the "y+" direction
 *
 * From the above, we can build the following state table where
 * the first entry is current total arrow destination, and the
 * second is the new bending operation at joint j:
 *
 *       states[Xp][Yp] = Yp;   ..
 *       states[Xp][Yn] = Yn;   ..
 *       states[Xp][Zp] = Zp;   ..
 *       states[Xp][Zn] = Zn;   ..
 *
 *       states[Xn][Yp] = Yn;   ~~
 *       states[Xn][Yn] = Yp;   ~~
 *       states[Xn][Zp] = Zn;   ~~
 *       states[Xn][Zn] = Zp;   ~~
 *
 *       states[Yp][Yp] = Xn;   @@
 *       states[Yp][Yn] = Xp;   %%
 *       states[Yp][Zp] = Yp;   ##
 *       states[Yp][Zn] = Yp;   ##
 *
 *       states[Yn][Yp] = Xp;   %%
 *       states[Yn][Yn] = Xn;   @@
 *       states[Yn][Zp] = Yn;   ##
 *       states[Yn][Zn] = Yn;   ##
 *
 *       states[Zp][Yp] = Zp;   ##
 *       states[Zp][Yn] = Zp;   ##
 *       states[Zp][Zp] = Xn;   @@
 *       states[Zp][Zn] = Xp;   %%
  *
 *       states[Zn][Yp] = Zn;   ##
 *       states[Zn][Yn] = Zn;   ##
 *       states[Zn][Zp] = Xp;   %%
 *       states[Zn][Zn] = Xn;   @@
 *
 * And from the above, we can minimize the state table even
 * further! Basically, we're _only_ interested in the bending
 * transitions that modify the global state. We can then
 * safely ignore all the neutral others. Thus, ignore all
 * changes marked with "##" above. Don't even check for them,
 * which means an implicit bypass.
 *
 * From the above, we can build the following rule set (by marks):
 *
 *   .. If current overall state is Xp, then new joint bend state always
 *      wins and become new overall state
 *
 *   ~~ If current overall state is Xn, then new overall state is always
 *      negative of requested bend state
 *
 *   @@ If new joint bending state is the same as overall state,
 *      new overall state is _always_ Xn (half, 180-degree, circle in
 *      the axises that matter, namely Y, and Z)
 *
 *   %% If new joint bending state is negative of current overall state,
 *      new overall state is _always_ Xp (earlier movement neutralized,
 *      in the axises that matter, namely Y and Z)
 */

int main(int argc, char **argv) {

        while (true) {
                int L;
                char wire_now[3], bend[3];

                scanf("%d", &L);
                if (L == 0)
                        break;

                strcpy(wire_now, "+x");
                while (L-- - 1) {
                        scanf("%s", bend);
                        if (strcmp(bend, "No") == 0)
                                continue;

                        if (strcmp(wire_now, "+x") == 0) {
                                strcpy(wire_now, bend);
                        } else if (strcmp(wire_now, "-x") == 0) {
                                flip_sign(bend);
                                strcpy(wire_now, bend);
                        } else if (strcmp(wire_now, bend) == 0) {
                                strcpy(wire_now, "-x");
                        } else if (different_sign(wire_now, bend))
                                strcpy(wire_now, "+x");
                }
                printf("%s\n", wire_now);
        }

        return 0;
}
