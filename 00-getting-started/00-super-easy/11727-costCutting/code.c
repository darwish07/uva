#include <stdio.h>

int main (int argc, char **argv) {
        int sets, i, a, b, c;

        scanf("%d", &sets);
        for (i = 1; i <= sets; i++) {
                scanf("%d %d %d", &a, &b, &c);

                printf("Case %d: ", i);
                if ((b < a && a < c) || (c < a && a < b))
                        printf("%d", a);
                else if ((a < b && b < c) || (c < b && b < a))
                        printf("%d", b);
                else
                        printf("%d", c);
                printf("\n");
        }

        return 0;
}

