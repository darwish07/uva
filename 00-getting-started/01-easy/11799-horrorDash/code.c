#include <stdio.h>

#define MAX_STUDENTS    100
int students_speed[MAX_STUDENTS];

int main(int argc, char **argv) {
        int tests, nstudents, max, i, j;

        scanf("%d", &tests);
        for (i = 1; i <= tests; i++) {
                scanf("%d", &nstudents);
                for (j = 0; j < nstudents; j++)
                        scanf("%d", &students_speed[j]);

                max = students_speed[0];
                for (j = 1; j < nstudents; j++)
                        if (students_speed[j] > max)
                                max = students_speed[j];

                printf("Case %d: %d\n", i, max);
        }

        return 0;
}
