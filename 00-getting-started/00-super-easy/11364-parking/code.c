#include <assert.h>
#include <stdio.h>

#define SHOPS_MAX   20
int shops_pos[SHOPS_MAX];

void scan(int *arr, int len, int *minval, int *maxval) {
        assert(len > 0);
        *minval = arr[0];
        *maxval = arr[0];

        int i;
        for (i = 1; i < len; i++) {
                if (arr[i] < *minval)
                        *minval = arr[i];
                if (arr[i] > *maxval)
                        *maxval = arr[i];
        }
}

int main(int argc, char **argv) {
        int sets, shops_count, min_shop_pos, max_shop_pos;
        int distance;

        scanf("%d", &sets);
        while (sets--) {
                scanf("%d", &shops_count);
                assert(shops_count <= SHOPS_MAX);

                int i;
                for (i = 0; i < shops_count; i++)
                        scanf("%d", &shops_pos[i]);

                /* Going and returning back */
                scan(shops_pos, shops_count, &min_shop_pos, &max_shop_pos);
                distance = (max_shop_pos - min_shop_pos) * 2;

                printf("%d\n", distance);
        }

        return 0;
}
