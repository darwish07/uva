#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define BUF_LEN	10000000
#define str(x)	# x
#define xstr(x) str(x)

char input_buf[BUF_LEN];
char output_buf[BUF_LEN * 2];
int j = 0;

int main(int argc, char ** argv) {
        char *ret;
        int len, segment_start, segment_stop, segment_len;
        bool left = true;

        while (true) {
                int ret = scanf("%" xstr(BUF_LEN) "[^\n]%n", input_buf, &len);
                if (ret <= 0 || ret == EOF)
                        break;

                if (!feof(stdin)) {
                        getchar();
                        input_buf[len++] = '\n';
                }
                input_buf[len] = '\0';

                int i = 0;
                while (i < len) {
                        segment_start = i;
                        while (input_buf[i] != '"' && i < len)
                                i++;
                        segment_stop = i;
                        segment_len = segment_stop - segment_start;

                        memcpy(&output_buf[j], &input_buf[segment_start], segment_len);
                        j += segment_len;

                        if (input_buf[i] == '"') {
                                if (left) {
                                        output_buf[j++] = '`';
                                        output_buf[j++] = '`';
                                } else {
                                        output_buf[j++] = '\'';
                                        output_buf[j++] = '\'';
                                }

                                i++;
                                left = !left;
                        }
                }
        }

        output_buf[j] = '\0';
        printf("%s", output_buf);
}
