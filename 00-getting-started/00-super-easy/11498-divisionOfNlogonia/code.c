#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

/*
 * (x, y)   => division point
 * (x1, y1) => query point
 */

int main(int argc, char **argv) {
        int npoints, x, y, x1, y1;

        while (true) {
                scanf("%d", &npoints);
                if (npoints == 0)
                        break;

                scanf("%d %d", &x, &y);

                while (npoints--) {
                        scanf("%d %d", &x1, &y1);
                        
                        if (x1 == x || y1 == y)
                                printf("divisa\n");
                        else if (x1 > x && y1 > y)
                                printf("NE\n");
                        else if (x1 < x && y1 > y)
                                printf("NO\n");
                        else if (x1 < x && y1 < y)
                                printf("SO\n");
                        else if (x1 > x && y1 < y)
                                printf("SE\n");
                        else
                                assert(false);
                }
                        
        }

        return 0;
}
