#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define NAME_MAX 82
char requirement[NAME_MAX], proposal[NAME_MAX];

char picked_proposal[NAME_MAX];
long picked_proposal_met_requirements;
double picked_proposal_price;

void pick(char *name, long nrequirements, double price) {
        strncpy(picked_proposal, name, NAME_MAX);
        picked_proposal_met_requirements = nrequirements;
        picked_proposal_price = price;
}

int main(int argc, char **argv) {
        long nrequirements, met_requirements, nproposals, casenr, i;
        double price;

        for (casenr = 1; ; casenr++) {
                scanf("%ld %ld\n", &nrequirements, &nproposals);
                if (nrequirements == 0 && nproposals == 0)
                        break;

                for (i = 0; i < nrequirements; i++)
                        gets(requirement);              /* No need; just consume it */

                picked_proposal_met_requirements = -1;
                picked_proposal_price = -1;
                while (nproposals--) {
                        gets(proposal);
                        scanf("%lf %ld\n", &price, &met_requirements);
                        for (i = 0; i < met_requirements; i++)
                                gets(requirement);      /* No need; just consume it */

                        assert(met_requirements <= nrequirements);
                        if (met_requirements > picked_proposal_met_requirements)
                                pick(proposal, met_requirements, price);

                        if (met_requirements == picked_proposal_met_requirements)
                                if (price < picked_proposal_price)
                                        pick(proposal, met_requirements, price);
                }

                if (casenr > 1) printf("\n");
                printf("RFP #%ld\n", casenr);
                printf("%s\n", picked_proposal);
        }

        return 0;
}
