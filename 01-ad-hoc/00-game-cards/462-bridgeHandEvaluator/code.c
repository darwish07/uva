/*
 * Accepted from first try. Cool ;-)
 */

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define min(a, b)       (((a) < (b)) ? (a) : (b))

enum rank {
        RANK_ACE = 0,
        RANK_TWO,
        RANK_THREE,
        RANK_FOUR,
        RANK_FIVE,
        RANK_SIX,
        RANK_SEVEN,
        RANK_EIGHT,
        RANK_NINE,
        RANK_TEN,
        RANK_JACK,
        RANK_QUEEN,
        RANK_KING,
        RANK_MAX,
};

enum suit {
        SUIT_SPADES = 0,
        SUIT_HEARTS,
        SUIT_DIAMONDS,
        SUIT_CLUBS,
        SUIT_MAX,
};

int rank_to_index(char rankc) {
        switch (rankc) {
        case 'A': return RANK_ACE;
        case '2': return RANK_TWO;
        case '3': return RANK_THREE;
        case '4': return RANK_FOUR;
        case '5': return RANK_FIVE;
        case '6': return RANK_SIX;
        case '7': return RANK_SEVEN;
        case '8': return RANK_EIGHT;
        case '9': return RANK_NINE;
        case 'T': return RANK_TEN;
        case 'J': return RANK_JACK;
        case 'Q': return RANK_QUEEN;
        case 'K': return RANK_KING;
        default : assert(false);
        }
}

int suit_to_index(char suitc) {
        switch (suitc) {
        case 'S': return SUIT_SPADES;
        case 'H': return SUIT_HEARTS;
        case 'D': return SUIT_DIAMONDS;
        case 'C': return SUIT_CLUBS;
        default : assert(false);
        }
}

char suit_to_char(int suit) {
        switch (suit) {
        case SUIT_SPADES:   return 'S';
        case SUIT_HEARTS:   return 'H';
        case SUIT_DIAMONDS: return 'D';
        case SUIT_CLUBS:    return 'C';
        default:            assert(false);
        }
}

/*
 * 2D array in the shap of a 1D one.
 * Columns are suits. Rows are the ranks.
 */
int cards[RANK_MAX][SUIT_MAX];
const int CARDS_NR = 13;

bool unique_rank_in_suit(int rank, int suit, int allowed_other_ranks_in_suit, bool up_to) {
        int i, sum;
        
        sum = 0;
        if (cards[rank][suit] == 1) {
                for (i = 0; i < rank; i++)
                        sum += cards[i][suit];
                for (i = rank + 1; i < RANK_MAX; i++)
                        sum += cards[i][suit];
                if (up_to) {
                        if (sum <= allowed_other_ranks_in_suit)
                                return true;
                } else {                        /* "at least" */
                        if (sum >= allowed_other_ranks_in_suit)
                                return true;
                }
        }

        return false;
}

/* A suit is stopped if it contains an ace, or if it contains a king
 * and at least one other card, or if it contains a queen and at least
 * two other cards */
int get_nr_stopped_suits(void) {
        int j, stopped_suits;

        stopped_suits = 0;
        for (j = 0; j < SUIT_MAX; j++) {
                if ((cards[RANK_ACE][j] == 1) ||
                    (unique_rank_in_suit(RANK_KING, j, 1, false)) ||
                    (unique_rank_in_suit(RANK_QUEEN, j, 2, false)))
                        ++stopped_suits;
        }
        return stopped_suits;
}

int main(int argc, char **argv) {
        int i, j, sum, ret, points;
        int matching_suits, picked_suit, cards_count, max_cards;

        while (true) {
                memset(cards, 0, RANK_MAX * SUIT_MAX * sizeof(cards[0][0]));
                for (i = 0; i < CARDS_NR; i++) {
                        char rankc, suitc;
                        ret = scanf("%c%c ", &rankc, &suitc);
                        if (ret == EOF) {
                                assert(i == 0);
                                goto out;
                        }
                        cards[rank_to_index(rankc)][suit_to_index(suitc)] = 1;
                }

#if 0
                /* Parser debugging: Print the whole cards array */
                for (i = 0; i < RANK_MAX; i++) {
                        for (j = 0; j < SUIT_MAX; j++) {
                                printf("%d ", cards[i][j]);
                        }
                        printf("\n");
                }
                printf("\n");
#endif

                points = 0;

                /* 1. Each ace counts 4 points. Each king counts 3 points.
                 * Each queen counts 2 points. Each jack counts one point. */
                for (j = 0; j < SUIT_MAX; j++) {
                        points += 4 * cards[RANK_ACE][j];
                        points += 3 * cards[RANK_KING][j];
                        points += 2 * cards[RANK_QUEEN][j];
                        points += 1 * cards[RANK_JACK][j];
                }

                /* 2. Subtract a point for any king of a suit in which the
                 * hand holds no other cards. */                
                for (matching_suits = 0, j = 0; j < SUIT_MAX; j++) {
                        if (unique_rank_in_suit(RANK_KING, j, 0, true))
                                ++matching_suits;
                }
                points += matching_suits * -1;

                /* 3. Subtract a point for any queen in a suit in which the
                 * hand holds only zero or one other cards. */
                for (matching_suits = 0, j = 0; j < SUIT_MAX; j++) {
                        if (unique_rank_in_suit(RANK_QUEEN, j, 1, true))
                                ++matching_suits;
                }
                points += matching_suits * -1;
                
                /* 4. Subtract a point for any jack in a suit in which the
                 * hand holds only zero, one, or two other cards */
                for (matching_suits = 0, j = 0; j < SUIT_MAX; j++) {
                        if (unique_rank_in_suit(RANK_JACK, j, 2, true))
                                ++matching_suits;
                }
                points += matching_suits * -1;

                /* One may open bidding in "no trump" if the hand evaluates
                 * to 16 or more points ignoring rules 5, 6, and 7 and if
                 * all four suits are stopped.
                 *
                 * A no trump bid is always __preferred__ over a suit bid
                 * when both are possible. */
                if (points >= 16 && get_nr_stopped_suits() == SUIT_MAX) {
                        printf("BID NO-TRUMP\n");
                        continue;
                }

                /* 5. Add a point for each suit in which the hand contains
                 * _exactly_ two cards.
                 *
                 * 6. Add two points for each suit in which the hand contains
                 * _exactly_ one card.
                 *
                 * 7. Add two points for each suit in which the hand contains
                 * no cards.
                 */
                for (j = 0; j < SUIT_MAX; j++) {
                        sum = 0;
                        for (i = 0; i < RANK_MAX; i++)
                                sum += cards[i][j];
                        if (sum == 2) {
                                points += 1;
                        } else if (sum == 1 || sum == 0) {
                                points += 2;
                        }
                }

                /* If the hand evaluates to fewer than 14 points, then the
                 * player must pass. */
                if (points < 14) {
                        printf("PASS\n");
                        continue;
                }

                /* If the hand evaluates to 14 or more points. Bidding is
                 * always opened in one of the suits with the most cards.
                 *
                 * If two suits or more have equal max cards, preference
                 * is 'S', 'H', 'D', 'C' in that order. */
                max_cards = 0;
                picked_suit = SUIT_MAX;
                for (j = 0; j < SUIT_MAX; j++) {
                        cards_count = 0;
                        for (i = 0; i < RANK_MAX; i++)
                                cards_count += cards[i][j];

                        if (cards_count == max_cards) {
                                picked_suit = min(picked_suit, j);
                        }
                        else if (cards_count > max_cards) {
                                picked_suit = j;
                                max_cards = cards_count;
                        }
                }

                assert(picked_suit != SUIT_MAX);
                printf("BID %c\n", suit_to_char(picked_suit));
        }

 out:
        return 0;
}
